# Statuts Scopyleft

<!-- Préambule

Quelques avertissements relatifs à ces statuts :

1. Nous choisissons ici de ne pas nous prémunir de la menace interne qui demanderait d’investir beaucoup d’energie à imaginer des moyens de défense. Nous préférons entretenir nos espaces de communication et protéger notre lien de confiance.
2. Il n’est pas obligatoire que des personnes avocat·es rédigent les statuts.
3. Ils sont une réponse à une contrainte légale et nous avons l’ambition qu’ils restent minimalistes.
4. Vous pouvez consulter/modifier/réutiliser la source du texte [par ici](https://gitlab.com/scopyleft/les-statuts/-/blob/main/statuts.md) (voir le [colophon](#comment-colophon)).
5. Chez Scopyleft, hormis les personnes en CDD, nous sommes toutes et tous co-gérant·e·s et associé·e·s.
6. La [dernière version en PDF](./static/2023-03-20-statuts-scopyleft.pdf).

-->

Pour rappel, tout ce qui n’est pas prévu dans les présents statuts est régi par les textes en vigueur.

## Article 1 - Forme

Société Coopérative de Production (SCOP) à Responsabilité Limitée régie par les textes suivants :

- la loi [n°78-763 du 19 juillet 1978][loi 78] ;
- la loi [n°47-1775 du 10 septembre 1947][loi 47] ;
- le [Chapitre III (Titre II, Livre II) du Code de commerce][chapitre III] ;
- les [articles 1832 à 1844-17 du Code Civil][articles 1832 a 1844].

[loi 78]: https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000339242/
[loi 47]: https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000684004/
[chapitre III]: https://www.legifrance.gouv.fr/codes/id/LEGISCTA000006146044/
[articles 1832 a 1844]: https://www.legifrance.gouv.fr/codes/id/LEGIARTI000006444158/


<!-- 
Précision :

On est ici plus exhaustif, en général on indique juste le livre 2 qui concerne les SARL et toutes les règles communes aux sociétés commerciales.

Et on intègre aussi les articles sur la variabilité du capital : L223-1, L223-43.
-->

## Article 2 - Dénomination

Scopyleft

## Article 3 - Siège social

199 Rue Hélène Boucher  
34170 Castelnau-le-Lez

Le transfert du siège social en tout autre endroit du territoire français est effectué par décision de la gérance et ratifié lors de la prochaine Assemblée Générale, le cas échéant la gérance est autorisée à modifier le présent article des statuts afin de mettre l’adresse du siège social à jour.

Tout acte et document émanant de la société et destiné aux tiers doit porter la dénomination sociale : « SCOP ARL à capital variable ».

<!-- Documentation
Par commodité et formalisme, il est recommandé de faire apparaitre dans les décisions extraordinaires le changement de siège social.
L’indiquer clairement permet de faciliter le travail des personnes aux greffes qui n’auront pas à chercher l’information.

Pour les SARL il n’y a pas de choix possible concernant ce sujet, c’est la loi qui fixe cette modalité.
Soit c’est une décision prise par la gérance ratifiée en AG, soit c’est décidé collectivement pendant une AG.

La ratification est permise par la lois qui s’est assouplie pour permettre au co gérance d’effectuer les démarches administratives sans attendre ou convoquer en AG.
-->

## Article 4 - Objet

<!-- Documentation
Il est recommandé de ne pas faire apparaître les valeurs dans les statuts — même en préambule — mais de le faire dans un document à part qui peut évoluer de manière autonome (sans déclaration au Greffe).

Il faut essayer d’être le plus précis possible pour accéder à un code NAF (garder la complexité/contexte en préambule). Si un code NAF est visé en particulier, il est recommandé de reprendre les termes de la descriptions pour coller au mieux aux attentes. 

Au final, on a décidé de mettre la définition qui nous convient et de voir quel code NAF nous sera attribué.
-->

### Définition

- Accompagner et co-piloter les activités innovantes de personnes physiques, de collectifs, d’entreprises et d’administrations ;
- Démarrer, inventer et promouvoir des activités dans un large spectre d’environnements.


### Mise en œuvre

L’activité commerciale et les différentes actions de l’entreprise permettent la réalisation concrète de cet objet. 
À titre indicatif, les domaines d’activités commerciales possibles sont (liste non-exhaustive) :

- Conception et développement de projets innovants, projets à impact environnemental le moins nuisible possible, projets à impact social dirigés vers la prise de soin ;
- Accompagnement à la création de structures (entreprises, associations…), accompagnement d’individus et de structures dans la réalisation de leurs projets ;
- Organisation d’évènements et d’actions de formation ;
- Accompagnement des structures et des personnes dans la transition vers une alimentation durable.

<!-- Documentation
Nous avons choisi de retirer de notre liste des éléments qui viendraient encore davantage élargir nos domaines d’actions et potentiellement perdre les greffes et/ou l’INSEE… L’idée est de regrouper autant que possible nos champs d’activités. Se limiter à 4 ou 5 gros blocs max.

C’est l’INSEE qui attribue un code APE (ou NAF) en fonction de l’interprétation qui est faites de l’objet de l’entreprise.
Ce code permet de faire des statistiques sur les activités des entreprises.

Les conventions collectives sont aussi liées à ces codes. Ça peut être un enjeu pour nous car les conventions collectives fixent des règles qui peuvent être bloquantes dans notre cas. Par exemple, sur la notion de lien de subordination.
Mais on peut relativiser cet enjeu car on peut rédiger des accords d’entreprises plus favorables.

-->

Et toutes activités annexes, connexes ou complémentaires s’y rattachant directement ou indirectement, ainsi que toutes opérations civiles, commerciales, industrielles, mobilières, immobilières, de crédit, utiles directement ou indirectement à la réalisation de l’objet social.

<!-- Documentation
Il est recommandé de terminer par le paragraphe ci-dessus pour permettre de réaliser toutes les activités connexes à nos activités principales (acheter des locaux, du matériel, etc.).

-->

## Article 5 - Mode d’administration

### Article 5.1 - Décisions qui sont réservées à l’Assemblée Générale

<!-- Documentation

cf. [article 7 de la loi de 1947][article 7]
cf. [article L223-27 du code du commerce][article L223-27]

[article 7]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000038590007/
[article L223-27]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799336/

Toutes les modifications statutaires 223-30, se votent en AG au minimum en majorité simple, pas à l’unanimité.

3 modes possibles de prise de décision :

* AG à la majorité simple
* Consultation par correspondance
* Acte unanime
-->

Les décisions relevant de la compétence des associé·e.s sont prises en Assemblée Générale.


### Article 5.2 - Choix du ou des co-gérant·e·s et pouvoirs des co-gérant·e·s

<!-- Documentation
Il faut souligner que l’article 1835 du code civil prévoit explicitement que les statuts prévoient les modalités de fonctionnement de la société, ce qui est d’ailleurs prévu également par l’[article 7 de la loi de 1947][article 7]. Ainsi, le memento Francis Lefebvre précise que les statuts doivent contenir toutes les autres clauses nécessaires pour assurer le bon fonctionnement de la société.

[article 7]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000038590007/

-->

#### Article 5.2.1 - Choix du ou des co-gérant·e·s

Les co-gérant·e·s peuvent être nommé·e·s à l’occasion d’une Assemblée Générale Extraordinaire, à l’occasion d’une décision des associé·e·s prises selon les modalités décrites dans l’article 5.7 des présents statuts. 

L’élection des co-gérant·e·s s’opère chaque année en Assemblée Générale Ordinaire.

<!-- Documentation
En l’absence de précision, les SCOP prévoient que les mandats soient renouvelés tous les 4 ans.

Une AGE requiert une majorité à 2/3 (vs AGO == majorité classique/simple).

-->

<!-- Commentaire

Il faut choisir pour la nommination entre AGO (majorité classique/simple) et AGE (majorité 2/3).
-->

#### Article 5.2.2 - Pouvoirs des co-gérant·e·s

Dans les rapports entre associé·e·s, les co-gérant·e·s peuvent faire tout acte de gestion dans l’intérêt de la société ([Articles L223-18][article L223-18] et [L223-29][article L223-29] du Code du Commerce).

[article L223-18]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031013015
[article L223-29]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799356

### Article 5.3 - Modalités du contrôle des opérations effectuées au nom des associé·e·s

Les actes de gestion font l’objet d’un contrôle des associé·e·s, lors de l’_Assemblée Générale Ordinaire_ annuelle conformément à l’[article 8 de la loi de 1947][article 8].

[article 8]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000035255562

### Article 5.4 - Modalités à suivre en cas de modification des statuts ou de dissolution de la SCOP

Les modifications des statuts sont effectuées conformément aux dispositions légales et font l’objet d’une approbation à l’occasion d’une _Assemblée Générale Extraordinaire_.

<!-- Documentation
Nécessité d’accord unanime des associé·e·s pour la modification des statuts : [article 1836 du Code Civil](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006444069)
-->

La dissolution de la SCOP s’opère conformément aux dispositions légales et notamment à l’[article 1844-5 du Code Civil][article 1844-5] et l’[article 5 de la Loi n° 78-763 du 19 juillet 1978][article 5].
Concernant la répartition de l’actif net subsistant après extinction du passif, il sera fait référence à l’[article 19 de la loi n° 47-1775 du 10 septembre 1947][article 19].

[article 1844-5]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006444165/
[article 5]: https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000339242/
[article 19]: https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000684004/

### Article 5.5 - Transmission des parts sociales

La transmission des parts sociales est régie par les [articles L223-13 à L223-17 du Code du commerce][articles L223-13 à L223-17].

[articles L223-13 à L223-17]: https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000005634379/LEGISCTA000006146044/#LEGIARTI000006223022-13


### Article 5.6 - Mode de consultation des associé·e·s

Les associé·e·s sont consulté·e·s à l’occasion des Assemblées Générales Ordinaires, Extraordinaires.

La SCOP fonctionne sur le principe d’une personne une voix : [Article de 14 de la loi de 1978][Article 14].

[Article 14]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000037823137


### Article 5.7 - Délibérations et majorité requise pour l’adoption des décisions collectives

<!-- Documentation
Le code du commerce déclarant que "toute clause exigeant une majorité plus élevée est réputée non écrite", on ne peux pas écrire que l’on prend des décisions à l’unanimité et espérer que ça produise un effet.

De fait, on ne peut pas augmenter la majorité.
Dans les SAS, on aurait plus de liberté de rédaction statutaire, on pourrait se permettre de mettre une gestion par consentement.

Ce qu’on nous propose :

Dans les statuts, on s’aligne sur le code du commerce et des sociétés et dans un document interne on explique davantage ce que l’on souhaite et comment on applique l’unanimité (ce que l’on va toujours chercher).
Ce document aura une valeur juridique entre nous, mais ne sera pas opposable par un tiers. On peut cependant le diffuser.
-->

En SARL, les décisions ordinaires ne sont pas soumises à un quorum et sont adoptées à la majorité des voix de la totalité des voix des associé·e·s qui peuvent être exprimées au sein de la société.

Le quorum et la majorité sont définis par les articles [L223-29][L223-29] et [L223-30][L223-30] du code du commerce.

[L223-29]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799356
[L223-30]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799345


<!-- Documentation
On peut préciser ici, qu’à l’exception de l’approbation des comptes, les décisions seront prises au cours de l’année et inscrites dans des procés verbaux en respectant l’article [L223-27][L223-27] du code du commerce. 

Toutefois, les statuts peuvent stipuler qu’à l’exception de celles prévues au premier alinéa de l’article L. 223-26, certaines des décisions pourront être prises par consultation écrite des associé·e·s (pas en SCOP ! Article 12 loi 78) ou pourront résulter du consentement de tous les associé·e·s exprimé dans un document écrit.

[L223-27]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799336/

-->

<!-- Documentation
Ce que prévoit la loi :

- Décisions ordinaires :
    - première consultation :
        - Quorum : aucune condition de quorum n’est exigée.
        - Majorité : les décisions de l’assemblée des associés doivent être prises par une majorité représentant plus de la moitié du nombre total d’associés.  
    - deuxième consultation :
Si la première assemblée n’a pu décider dans les conditions fixées au premier alinéa, une seconde assemblée sera réunie et les décisions seront prises à la majorité des présents ou représentés.  

Les décisions concernant la nomination ou la révocation du gérant ou de la gérante sont toujours prises à la majorité absolue de l’ensemble des associé·e·s et à bulletins secrets.  

- Décisions extraordinaires  
    - première consultation :  
        - Quorum : les trois quarts du total des droits de vote.
        - Majorité : les trois quarts du total des droits de vote présents ou représentés.
  
    - Deuxième consultation :  
        - Quorum : la moitié du total des droits de vote.
        - Majorité : les trois quarts du total des droits de vote présents ou représentés.


Les modifications des statuts sont décidées par une majorité représentant les trois quarts du total des droits de vote présents ou représentés.

**Compétences de l’assemblée ordinaire**
L’assemblée ordinaire annuelle des associé.e.s, le cas échéant réunie extraordinairement pour examiner les questions dont la solution ne souffre pas d’attendre la prochaine assemblée annuelle, exerce les pouvoirs qui lui sont conférés par la loi et notamment :
- approuve ou redresse les comptes ;
- ratifie la répartition des bénéfices si une pré-répartition a été décidée par la gérance ;
- prononce, dans les conditions prévues aux statuts, l’admission des associé.e.s ;
- nomme à la gérance, contrôle sa gestion et en révoque les gérant.e.s ;
- s’il y a lieu, nomme et révoque les membres du conseil de surveillance ;
- approuve les conventions passées entre la Société et les associé.e.s ;
- décide ou ratifie la répartition des bénéfices et peut décider la conversion en parts sociales des répartitions revenant aux associé.e.s ;
- délibère sur toutes questions portées à l’ordre du jour n’emportant pas modification des statuts ;
désigne le ou la réviseur.e et son ou sa suppléant.e figurant sur la liste des réviseur.e.s agréé.e.s ;
- décide les émissions de titres participatifs.

**Compétences de l’assemblée extraordinaire :**
L’assemblée des associé·es a compétence pour modifier les statuts, mais ne peut augmenter les engagements des associé·es sans leur accord unanime, sauf le cas particulier de l’engagement de souscription au capital expressément prévu par la loi du 19 juillet 1978. Elle peut notamment décider ou autoriser, sans que l’énumération ait un caractère limitatif :
- l’exclusion d’un·e associé·e qui aurait causé un préjudice matériel ou moral à la Société ;
- la modification de la dénomination sociale ;
- la prorogation ou la dissolution anticipée de la Société ;
- la fusion de la Société.
-->

<!--
Le _Journal de Décisions_ consigne les décisions prises par les associé·e·s.
Finalement, on doit faire une AGE pour le Greffe donc autant le rendre explicite.
-->
<!-- Documentation
La tenue du registre des décisions relève de la responsabilité de la gérance. L’absence de tenue du registre constitue une faute de gestion qui est susceptible d’engager la responsabilité civile du/des gérant(s) lorsque la faute commise a entrainé un préjudice à la société.
-->

### Article 5.8 - Limitation de procuration

Le nombre de procurations détenues par un·e même associé·e ne peut être supérieur à un, conformément à l’[article 14 de la loi 78][article 14].

[article 14]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000029321011

<!-- Documentation
À prendre en compte si on devient plus de vingt :
« d’un nombre de voix excédant le vingtième des associés lorsqu’elle comprend vingt membres ou plus ».
-->

## Article 6 - Conditions d’adhésion, d’agrément, de retrait, d’exclusion et de radiation des associé·e·s

L’adhésion, l’agrément, le retrait, l’exclusion et la radiation des associé·e·s font l’objet d’une décision prise à l’occasion d’une _Assemblée Générale Extraordinaire_ des associé·e·s. 

Seul·e·s les salarié·e·s de la SCOP peuvent devenir associé·e·s.

L’adhésion du ou de la salarié·e à la SCOP est possible après 6 mois de salariat sur demande volontaire du ou de la salarié·e. L’adhésion devient obligatoire pour tout·e salarié·e au bout de 18 mois.

Un·e salarié·e quittant la SCOP perd sa voix dès la fin de son contrat de travail.

Pour le calcul de la valeur de remboursement de la part sociale, il est convenu que les pertes qui apparaissent à la clôture de l’exercice s’imputent pour partie sur les réserves statutaires et pour partie sur le capital.

<!-- Documentation
Ce que dit la loi, [article 8 de la Loi n° 78-763 du 19 juillet 1978][article 8] portant statut des sociétés coopératives de production.

[article 8]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000029321329
-->
<!-- Documentation
Ne considérant pas la menace interne (cf. [Préambule][#comment-preambule]), les conditions de retrait et d’exclusion ne sont pas considérées sans discussion collective préalable.
-->
<!-- Documentation

Il faut choisir ce qui influe sur la valeur de la part de remboursement.
Chaque année, cette valeur doit être réévaluée.

* S’il n’y a pas de perte, remboursement à la valeur nominale.
* S’il y a une perte, ça peut aller piocher dans la réserve pour rembourser à la valeur nominale, ça peut-être moitié-moitié ou complètement dans le capital (et donc l’associé·e perd de l’argent).

La CGSCOP propose l’option moitié-moitié par défaut.

C’est un choix politique à faire plus que financier.

Les réserves statutaires sont principalement constituées par le fonds de développement. Le montant des pertes à imputer sur le capital se calcule selon la formule suivante : Perte x (capital / capital + réserves statutaires).

* le montant du capital à retenir est celui du dernier jour de l’exercice auquel a été réintégré le montant du capital qui était détenu par les associés sortants ;
* les réserves statutaires sont celles inscrites au bilan au dernier jour de l’exercice.
-->


## Article 7 - Étendue et modalités de la responsabilité de chacun·e des associé·e·s dans les engagements de la Scop

<!-- Documentation
« leur apport » correspond à la participation personnelle au capital (parts).
-->

Les associé·e·s de la SCOP ne supportent les pertes qu’à concurrence de leur apport conformément à l’[article L-223-1 alinéa 1 du Code de commerce][article L-223-1].

[article L-223-1]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000019291708/

## Article 8 - Participation des associé·e·s à l’Assemblée Générale par visioconférence

Les associé·e·s qui participent aux _Assemblées Générales_ par visioconférence (ou par des moyens de télécommunication permettant leur identification et dont la nature et les conditions d’application sont déterminées par l’[article L223-27 du Code de commerce][article L223-27]) sont réputé·e·s présent·e·s pour le calcul du quorum et de la majorité.

[article L223-27]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006223182/2005-08-03

<!-- Documentation
Voir aussi [article 10] de la loi de 1947.
Voir aussi l’article de la [loi 78] qui va bien, et c’est pas le 225-107 pour les SARL.

[article 10]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000029320806
-->

## Article 9 - Capital social initial et apports

### Article 9.1 - Dépôt des fonds

Le dépôt des fonds constituant le capital social a été effectué sur un compte bancaire à la Nef/Crédit Coopératif le 14 décembre 2012.

<!-- Documentation

C’est une obligation légale prévue par le code de commerce qui est utile car elle permet aux créanciers et créancières de savoir où se trouve la trésorerie de l’entreprise.

Voir [Article R22-10-3].

[Article R22-10-3]: https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000005634379/LEGISCTA000042922237/2021-08-22
-->

### Article 9.2 - Montant du capital

Le montant du capital social initial était de 12&#8239;000 € divisés en 200 parts de 60 €.

Au 1<sup>er</sup> septembre 2022, le capital social est de 3&#8239;540 € divisés en 59 parts de 60 € chacunes réparties entre les associé·e·s comme telles :

- M. David LARLET (propriétaire de 18 parts) ;
- M. Vincent AGNANO (propriétaire de 18 parts) ;
- M. Stéphane LANGLOIS (propriétaire de 18 parts) ;
- Mme. Sarah RUBIO (propriétaire de 1 part) ;
- Mme. Audrey BRAMY (propriétaire de 1 part) ;
- Mme. Juliette PALUMBO (propriétaire de 1 part) ;
- Mme. Violaine ANDRIEUX (propriétaire de 1 part) ;
- Mme. Emma LACROIX (propriétaire de 1 part).

<!-- Documentation
La répartition des parts sociales est mentionnée dans les statuts. — [article 7 du code du commerce][article 7]
[article 7]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006222923/2022-08-17
-->

### Article 9.3 - Montant en dessous duquel le capital ne peut être réduit par les reprises d’apports

<!-- Documentation
Obligation de préciser le montant en dessous duquel le capital ne peut être réduit [l’article L. 231-5 du Code du commerce][L231-5].
 
13380€ (capital max en août 2022) / 4 (un quart) = 3345€ 
On ne peut pas avoir un capital en-dessous de cette valeur.

[L231-5]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006228809/2000-09-21
-->
 
Le montant du capital ne peut être réduit par les reprises d’apports prévus par l’[article L231-1 du Code du commerce][article L231-1] en dessous de ce que prévoit l’[article 13 de la loi de 1947][article 13].

Le capital maximum atteint a été de 13380 euros en août 2022.

[article L231-1]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006228803/
[article 13]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000027513282/


### Article 9.4 - Libération des parts

La libération d’au moins une part sociale doit être effectuée immédiatement à l’acquisition du statut d’associé.e. 

La libération de parts supplémentaires de la part des associé.e.s se fait sur demande et sur l’envie personnelle. L’associé.e prenant de nouvelles parts les libère immédiatement après sa demande. 

<!-- Cela nous laisse le temps de régler administrativement la situation en cas d’arrivée/départ via l’AG Ordinaire annuelle. -->

<!-- Documentation

> Les parts sociales des coopératives qui sont constituées sous le régime de la présente loi doivent être libérées d’un quart au moins au moment de leur souscription et la libération du surplus doit être effectuée dans les délais fixés par les statuts ==sans pouvoir excéder cinq ans== à partir de la date à laquelle la souscription est devenue définitive. — [Article 12 de la loi de 47][Article 12 de la loi de 47]

[Article 12 de la loi de 47]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000006288739

-->

### Article 9.5 - Variabilité du capital social

Le capital social est susceptible d’augmentation par des versements successifs des associé·e·s ou l’admission d’associé·e·s nouveaux et nouvelles et de diminution par la reprise totale ou partielle des apports effectués.

Il n’est pas obligatoire de préciser le montant maximal du capital, comme indiqué dans l’[article 7 du 10 septembre 1947].

[article 7 du 10 septembre 1947]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000038590007

<!--
On précise qu’il n’est pas obligatoire d’indiquer le montant max en citant l’article car, bien souvent, le Greffe ne connait pas bien les articles qui régissent les SCOP.
-->

## Article 10 - Possibilité de représentation d’un·e associé·e par une autre personne

Seul·e un·e autre associé·e de la SCOP peut représenter un·e associé·e à l’occasion d’une Assemblée Générale si celui ou celle-ci lui en fait la demande. 

## Article 11 - Répartition du résultat

La clé de répartition est variable. La décision de répartition est prise par les co-gérant·e·s avant la clôture de l’exercice et communiquée aux associé·e·s lors d’une Assemblée Générale Ordinaire réunie extraordinairement ou par lettre remise en main propre contre décharge. Elle est ratifiée par l’Assemblée Générale Ordinaire appelée à statuer sur les comptes de l’exercice.

La répartition est effectuée conformément à [l’article 33 de la loi 78][article 33].

La répartition de la part travail entre les bénéficiaires s’opère uniquement au prorata du temps de travail fourni.

[article 33]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000029320978

<!-- Documentation

Deux possibilités : 

- Une variante variable (par défaut), nécessite de s’accorder chaque fin d’année sur la clé de répartition à adopter sur le résultat : part travail (max. 84%) vs. réserve.
- Une variante avec une clé fixe qui doit être écrite dans les statuts (option non retenue ici).

Dans la grande majorité des cas, pour les SCOP en création, on laisse l’AG fixer la clé de répartition (part travail et réserve ne sont pas soumises à impôt).

Une fois que la réserve est suffisamment remplie et qu’elle permet la pérennité de l’entreprise, alors on peut la fixer à 84/16 qui permet de donner le maximum aux salarié·es et plus tellement en réserve.

La part travail est accessible aux salarié·e·s au-delà de 3 mois passés dans l’entreprise au cours de l’exercice social.
-->

## Article 12 - Dates d’ouverture et de clôture des exercices sociaux

L’exercice s’ouvre le 1<sup>er</sup> janvier et se clôture le 31 décembre.

## Article 13 - Durée de la société

La durée de la société est fixée à 99 ans.

## Article 14 - Arbitrage

Toutes les contestations qui pourraient s’élever au cours de la vie de la Société ou de sa liquidation seront soumises à la Commission d’arbitrage de la Confédération générale des Scop. Les contestations concernées sont celles pouvant s’élever :

- entre les associé·e·s ou ancien·ne·s associé·e·s eux-mêmes ou elles-mêmes au sujet des affaires sociales, notamment de l’application des présents statuts et tout ce qui en découle, ainsi qu’au sujet de toutes les affaires traitées entre la Société et ses associé·e·s ou ancien·ne·s associé·e·s ;
- entre la Société et une autre Société, soit au sujet des affaires sociales ou de toute autre affaire traitée.

La présente clause vaut compromis d’arbitrage.

Le règlement d’arbitrage est remis aux parties lors de l’ouverture de la procédure. Les sentences arbitrales sont exécutoires et susceptibles d’appel devant la Cour d’Appel de Paris.

<!-- Documentation
Indispensable pour adhérer à la CGScop : 

On doit insérer la clause d’arbitrage qui permet de soustraire tout litige aux commissions étatiques et de la soumettre à la CGScop.

En première instance, on n’irait pas au tribunal mais on irait devant la commission de la CGScop qui est compétente pour tous les litiges qui relèvent des affaires coopératives. Les juges sont souvent peu habitués, d’où l’intérêt de faire appel à la CGSCop.

Nous avons un mode de résolution des conflits qui nous est propre mais il est important de rappeler que nous avons cette opportunité.
-->

## Article 15 - Adhésion a la Confédération générale des Scop

La Société adhère à la Confédération Générale des Scop, association régie par la loi du 1<sup>er</sup> juillet 1901 dont le siège est à Paris 17<sup>e</sup>, 37 rue Jean Leclaire, chargée de représenter le Mouvement Coopératif et de la défense de ses intérêts, à l’Union Régionale des Scop territorialement compétente et à la Fédération professionnelle dont la Société relève.

<!-- Documentation
Doit être ajoutée si on adhère à la CG SCOP et URSCOP
-->

<!-- Colophon

Ces statuts ont été écrits en markdown, les commentaires sont des commentaires HTML qui nous permettent de pouvoir avoir un rendu final pour le Greffe qui soit propre et en parallèle de produire ce document commenté pour partager notre expérience/savoir dans le domaine (et ne pas oublier nous-même !).

-->

# Les statuts

Documentation autour de l’évolution des statuts de Scopyleft.

## Installation

Pré-requis : Python3

Installer et activer un environnement virtuel :

    $ python3 -m venv venv
    $ source venv/bin/activate

Installer les dépendances :

    $ make install


## Construction

Lancer la commande :

    $ make build


## Lancement

Lancer le serveur local :

    $ make serve

Aller sur http://127.0.0.1:8000/

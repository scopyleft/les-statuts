.DEFAULT_GOAL := help

.PHONY: install
install: ## Install the dependencies
	python -m pip install -r requirements.txt


.PHONY: dev
dev: ## Install the development dependencies
	python -m pip install -r requirements-dev.txt


.PHONY: build
build: ## Generate the site
	@cp -R static/ public/static/
	@python greffe.py build


.PHONY: serve
serve: ## Launch a local server to serve the `public` folder.
	@python3 -m http.server 8000 --bind 127.0.0.1 --directory public


.PHONY: help
help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

# See https://daniel.feldroy.com/posts/autodocumenting-makefiles
define PRINT_HELP_PYSCRIPT # start of Python section
import re, sys

output = []
# Loop through the lines in this file
for line in sys.stdin:
    # if the line has a command and a comment start with
    #   two pound signs, add it to the output
    match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
    if match:
        target, help = match.groups()
        output.append("\033[36m%-10s\033[0m %s" % (target, help))
# Sort the output in alphanumeric order
output.sort()
# Print the help result
print('\n'.join(output))
endef
export PRINT_HELP_PYSCRIPT # End of python section
